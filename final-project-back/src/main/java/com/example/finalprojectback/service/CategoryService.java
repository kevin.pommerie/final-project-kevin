package com.example.finalprojectback.service;

import com.example.finalprojectback.controller.CategoryDto;
import com.example.finalprojectback.domain.Category;
import com.example.finalprojectback.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Transactional
    public List<Category> getCategories() { return this.categoryRepository.findAll(); }

    @Transactional
    public Optional<Category> getCategoryById(Integer id) {
        return this.categoryRepository.findById(id);
    }

    @Transactional
    public Category addCategory(CategoryDto categoryDto) {
        Category category = categoryDto.toDomain();
        return this.categoryRepository.save(category);
    }

    @Transactional
    public void deleteCategory(Integer id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Category doesn't exist, can't delete it"));

        categoryRepository.delete(category);
    }
}
