package com.example.finalprojectback.service;

import com.example.finalprojectback.controller.StudentDto;
import com.example.finalprojectback.domain.Student;
import com.example.finalprojectback.repository.StudentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents(){
        return this.studentRepository.findAll();
    }

    @Transactional
    public Optional<Student> getStudentById(Integer id){
        return this.studentRepository.findById(id);
    }

    @Transactional
    public Student addStudent(StudentDto studentDto) {
        Student student = studentDto.toDomain();
        return this.studentRepository.save(student);
    }

    @Transactional
    public void deleteStudent(Integer id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Student doesn't exist, can't delete it"));

        studentRepository.delete(student);
    }
}
