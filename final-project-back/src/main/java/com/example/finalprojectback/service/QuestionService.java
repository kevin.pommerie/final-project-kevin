package com.example.finalprojectback.service;

import com.example.finalprojectback.controller.QuestionDto;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.repository.CategoryRepository;
import com.example.finalprojectback.repository.QuestionRepository;
import com.example.finalprojectback.repository.StudentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {
    private final QuestionRepository questionRepository;
    private final CategoryRepository categoryRepository;
    private final StudentRepository studentRepository;

    public QuestionService(QuestionRepository questionRepository, CategoryRepository categoryRepository, StudentRepository studentRepository){
        this.questionRepository = questionRepository;
        this.categoryRepository = categoryRepository;
        this.studentRepository = studentRepository;
    }

    @Transactional
    public List<Question> getQuestions(){
        return  this.questionRepository.findAll();
    }

    @Transactional
    public Optional<Question> getQuestionById(Integer id) {
        return this.questionRepository.findById(id);
    }

    @Transactional
    public Question addQuestion(QuestionDto questionDto) {
        Question question = questionDto.toDomain();
        return this.questionRepository.save(question);
    }

    @Transactional
    public void deleteQuestion(Integer id) {
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Question doesn't exist, can't delete it"));

        questionRepository.delete(question);
    }
}
