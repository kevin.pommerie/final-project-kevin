package com.example.finalprojectback.service;

import com.example.finalprojectback.controller.AnswerDto;
import com.example.finalprojectback.domain.Answer;
import com.example.finalprojectback.repository.AnswerRepository;
import com.example.finalprojectback.repository.QuestionRepository;
import com.example.finalprojectback.repository.StudentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerService {
    private final QuestionRepository questionRepository;
    private final StudentRepository studentRepository;
    private final AnswerRepository answerRepository;

    public AnswerService(QuestionRepository questionRepository, StudentRepository studentRepository, AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.studentRepository = studentRepository;
        this.answerRepository = answerRepository;
    }

    @Transactional
    public List<Answer> getAnswers() {
        return this.answerRepository.findAll();
    }

    @Transactional
    public Optional<Answer> getAnswerById(Integer id) {
        return this.answerRepository.findById(id);
    }

    @Transactional
    public Answer addAnswer(AnswerDto answerDto) {
        Answer answer = answerDto.toDomain();
        return this.answerRepository.save(answer);
    }

    @Transactional
    public void deleteAnswer(Integer id) {
        Answer answer = answerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Answer doesn't exist, can't delete it"));

        answerRepository.delete(answer);
    }
}
