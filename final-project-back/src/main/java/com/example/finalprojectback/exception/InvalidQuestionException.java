package com.example.finalprojectback.exception;

public class InvalidQuestionException extends InvalidException {

    public InvalidQuestionException() {}

    public InvalidQuestionException(String message) {
        super(message);
    }

    public InvalidQuestionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidQuestionException(Throwable cause) {
        super(cause);
    }

}
