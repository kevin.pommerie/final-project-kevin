package com.example.finalprojectback.exception;

public class InvalidAnswerException extends InvalidException {
    public InvalidAnswerException() {}

    public InvalidAnswerException(String message) {
        super(message);
    }

    public InvalidAnswerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAnswerException(Throwable cause) {
        super(cause);
    }
}
