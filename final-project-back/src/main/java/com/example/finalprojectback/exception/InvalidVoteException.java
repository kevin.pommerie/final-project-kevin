package com.example.finalprojectback.exception;

public class InvalidVoteException extends InvalidException {

    public InvalidVoteException() {}

    public InvalidVoteException(String message) {
        super(message);
    }

    public InvalidVoteException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidVoteException(Throwable cause) {
        super(cause);
    }
}
