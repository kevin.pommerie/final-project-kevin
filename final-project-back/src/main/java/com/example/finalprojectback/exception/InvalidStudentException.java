package com.example.finalprojectback.exception;

public class InvalidStudentException extends InvalidException {
    public InvalidStudentException() {}

    public InvalidStudentException(String message) {
        super(message);
    }

    public InvalidStudentException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidStudentException(Throwable cause) {
        super(cause);
    }

    public InvalidStudentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
