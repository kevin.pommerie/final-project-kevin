package com.example.finalprojectback.exception;

public class NotFoundException extends FunctionalException {

    private final Integer id;
    private final String string;
    private final Class<?> entityClass;

    public NotFoundException(Class<?> entityClass, Integer id) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found");
        this.id = id;
        this.string = "";
        this.entityClass = entityClass;
    }

    public NotFoundException(Class<?> entityClass, String string) {
        super(entityClass.getSimpleName() + " with string " + string + " is not found");
        this.id = null;
        this.string = string;
        this.entityClass = entityClass;
    }

    public NotFoundException(Class<?> entityClass, Integer id, Throwable cause) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found", cause);
        this.id = id;
        this.string = "";
        this.entityClass = entityClass;
    }

    public Integer getId() {
        return id;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }
}
