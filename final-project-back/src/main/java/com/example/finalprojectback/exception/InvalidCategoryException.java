package com.example.finalprojectback.exception;

public class InvalidCategoryException extends InvalidException {
    public InvalidCategoryException() {}

    public InvalidCategoryException(String message) {
        super(message);
    }

    public InvalidCategoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCategoryException(Throwable cause) {
        super(cause);
    }
}
