package com.example.finalprojectback.domain.exception;

import com.example.finalprojectback.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.InvalidObjectException;

@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(InvalidObjectException.class)
    public ResponseEntity<ErrorHttp> onInvalidSomethingException(InvalidObjectException e) {
        LOGGER.info("objet invalide");
        return ResponseEntity.badRequest().body(new ErrorHttp(e.getMessage()));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorHttp> onNotFoundException(NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorHttp("Not found"));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorHttp> onUnexpectedException(Exception e) {
        LOGGER.error("Unexpected exception", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorHttp("Unexpected exception"));
    }

    public static class ErrorHttp {
        private final String message;

        public ErrorHttp(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}