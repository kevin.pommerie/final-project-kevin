package com.example.finalprojectback.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        //use https://www.javainuse.com/onlineBcrypt for bcrypt passwords
        auth.inMemoryAuthentication()
                .withUser("admin")
                //.password("{bcrypt}$2a$10$oXfPVcjf02ebJB6bIl1aYeq2DWwNClAt9zdpEUX9BmnwTJoJXWFoS")
                .password(passwordEncoder().encode("password"))
                .roles("student", "admin")
                .and()
                .withUser("testuser")
                //.password("{bcrypt}$2a$10$iQRb5KFWDHx74knq5QpeMe5k8ImGnmp114xtX5EExQEYjWFpT8sji")
                .password(passwordEncoder().encode("password"))
                .roles("student");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/questions").permitAll()
                .antMatchers(HttpMethod.POST, "/questions").hasRole("student")
                .antMatchers("/users").hasRole("admin")
                .antMatchers("/profile").hasAnyRole("student", "admin")
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                //.and()
                //.formLogin()
                .and()
                .httpBasic()
                .and()
                .cors()
                .and()
                .csrf().disable();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }
}
