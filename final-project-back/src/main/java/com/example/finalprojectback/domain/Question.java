package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidQuestionException;

import javax.persistence.*;

@Entity
@Table(name = "questions")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_category")
    private Category category;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_student")
    private Student student;

    public Question(String title, String content, Category category, Student student){
        this.title = title;
        this.content = content;
        this.category = category;
        this.student = student;
    }

    public Question(){
        this.title = "title";
        this.content = "content";
        this.category = new Category();
        this.student = new Student();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(title.equals("")){
            throw new InvalidQuestionException();
        }
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        if(content.equals("")){
            throw new InvalidQuestionException();
        }
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        if(category == null){
            throw new InvalidQuestionException();
        }
        this.category = category;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        if(student == null){
            throw new InvalidQuestionException();
        }
        this.student = student;
    }
}
