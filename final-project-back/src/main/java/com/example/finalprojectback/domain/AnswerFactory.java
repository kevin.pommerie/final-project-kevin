package com.example.finalprojectback.domain;

public class AnswerFactory {
    public static Answer solution(){
        return new Answer(new Student(), new Question(), "this is my answer");
    }
}
