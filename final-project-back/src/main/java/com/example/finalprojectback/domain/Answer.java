package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidAnswerException;

import javax.persistence.*;

@Entity
@Table(name = "answers")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_student")
    private Student student;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_question")
    private Question question;

    @Column(name = "content")
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        if(student == null){
            throw new InvalidAnswerException();
        }
        this.student = student;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        if(question == null){
            throw new InvalidAnswerException();
        }
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        if(content.equals("")){
            throw new InvalidAnswerException();
        }
        this.content = content;
    }

    public Answer(Student student, Question question, String content){
        this.student = student;
        this.question = question;
        this.content = content;
    }

    public Answer(){
        this.student = new Student();
        this.question = new Question();
        this.content = "content";
    }
}
