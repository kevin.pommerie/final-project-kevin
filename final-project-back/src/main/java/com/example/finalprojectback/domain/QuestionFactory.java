package com.example.finalprojectback.domain;

public class QuestionFactory {

    public static Question howdoi(){
        return new Question("title", "content", new Category(), new Student());
    }
}
