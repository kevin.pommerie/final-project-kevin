package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidCategoryException;
import com.example.finalprojectback.exception.InvalidStudentException;

import javax.persistence.*;

@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public Category(String name){
        this.name = name;
    }

    public Category(){
        this.name = "name";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.equals("")){
            throw new InvalidCategoryException();
        }
        this.name = name;
    }
}
