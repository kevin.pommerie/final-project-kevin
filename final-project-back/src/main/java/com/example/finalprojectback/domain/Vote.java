package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidStudentException;
import com.example.finalprojectback.exception.InvalidVoteException;

import javax.persistence.*;

@Entity
@Table(name = "votes")
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_answer")
    private Answer answer;

    @Column(name = "votevalue")
    private Integer votevalue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        if(answer == null){
            throw new InvalidVoteException();
        }
        this.answer = answer;
    }

    public Integer getVotevalue() {
        return votevalue;
    }

    public void setVotevalue(Integer votevalue) {
        if(!votevalue.equals(1) && !votevalue.equals(-1)){
            throw new InvalidVoteException();
        }
        this.votevalue = votevalue;
    }

    public Vote(Answer answer, Integer votevalue){
        this.answer = answer;
        this.votevalue = votevalue;
    }

    public Vote(){
        this.answer = new Answer();
        this.votevalue = -1;
    }
}
