package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidAnswerException;
import com.example.finalprojectback.exception.InvalidStudentException;

import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "pseudonym")
    private String pseudonym;

    @Column(name = "email_address")
    private String email_address;

    @Column(name = "password")
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        if(first_name.equals("")){
            throw new InvalidStudentException();
        }
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        if(last_name.equals("")){
            throw new InvalidStudentException();
        }
        this.last_name = last_name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        if(pseudonym.equals("")){
            throw new InvalidStudentException();
        }
        this.pseudonym = pseudonym;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        if(email_address.equals("")){
            throw new InvalidStudentException();
        }
        this.email_address = email_address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(password.equals("")){
            throw new InvalidStudentException();
        }
        this.password = password;
    }

    public Student(String first_name, String last_name, String pseudonym, String email_address, String password){
        this.first_name = first_name;
        this.last_name = last_name;
        this.pseudonym = pseudonym;
        this.email_address = email_address;
        this.password = password;
    }

    public Student(){
        this.first_name = "first_name";
        this.last_name = "last_name";
        this.pseudonym = "pseudonym";
        this.email_address = "email_address";
        this.password = "password";
    }
}
