package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Answer;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.domain.Student;

public class AnswerDto {
    private int id;
    private Student student;
    private Question question;
    private String content;

    public AnswerDto() {
    }

    public static AnswerDto fromDomain(Answer answer) {
        AnswerDto dto = new AnswerDto();
        dto.setId(answer.getId());
        dto.setStudent(answer.getStudent());
        dto.setQuestion(answer.getQuestion());
        dto.setContent(answer.getContent());

        return dto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Answer toDomain() {
        Answer answer = new Answer();
        answer.setId(this.id);
        answer.setStudent(this.student);
        answer.setQuestion(this.question);
        answer.setContent(this.content);

        return answer;
    }
}