package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.repository.AnswerRepository;
import com.example.finalprojectback.repository.QuestionRepository;
import com.example.finalprojectback.service.AnswerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class AnswerController {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final AnswerService answerService;

    public AnswerController(QuestionRepository questionRepository, AnswerRepository answerRepository, AnswerService answerService){
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.answerService = answerService;
    }

    @GetMapping("/questions/{question_id}/answers")
    public List<AnswerDto> getAnswers(@PathVariable Integer question_id){
        Optional<Question> question = questionRepository.findById(question_id);

        return answerRepository.findAllByQuestion(question).stream()
                .map(AnswerDto::fromDomain)
                .collect(Collectors.toList());
    }

}
