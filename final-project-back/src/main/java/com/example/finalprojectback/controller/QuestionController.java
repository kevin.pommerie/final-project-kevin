package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.exception.NotFoundException;
import com.example.finalprojectback.repository.QuestionRepository;
import com.example.finalprojectback.service.QuestionService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static com.example.finalprojectback.repository.QuestionSpecifications.*;

@CrossOrigin
@RestController
public class QuestionController {

    private final QuestionRepository questionRepository;
    private final QuestionService questionService;

    public QuestionController(QuestionRepository questionRepository, QuestionService questionService){
        this.questionRepository = questionRepository;
        this.questionService = questionService;
    }

    private static Specification<Question> fromFilter(QuestionFilterDto filter) {
        return hasId(filter.getId())
                .and(hasTitle(filter.getTitle()))
                .and(hasContent(filter.getContent()))
                .and(hasCategory(filter.getCategory()))
                .and(hasStudent(filter.getStudent()));
    }

    @GetMapping("/questions")
    public PageDto<QuestionDto> getQuestions(@SortDefault(value = {"id"}) Pageable pageable, QuestionFilterDto filter) {
        //Specification<Question> questionSpecification = fromFilter(filter);
        return PageDto.fromDomain(questionRepository.findAll(pageable), QuestionDto::fromDomain);
    }

    @GetMapping("/questions/{id}")
    public QuestionDto getQuestionById(@PathVariable Integer id) {
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Question.class, id));

        return QuestionDto.fromDomain(question);
    }

    @PostMapping("/questions")
    public ResponseEntity<QuestionDto> addQuestion(@RequestBody QuestionDto questionDto) {
        Question newQuestion = questionService.addQuestion(questionDto);

        URI location = URI.create("/questions" + newQuestion.getId());
        return ResponseEntity.created(location)
                .body(QuestionDto.fromDomain(newQuestion));
    }

    @DeleteMapping("/questions/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteQuestion(@PathVariable Integer id) { questionService.deleteQuestion(id); }
}
