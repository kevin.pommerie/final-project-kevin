package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Student;

public class StudentDto {

    private int id;
    private String first_name;
    private String last_name;
    private String pseudonym;
    private String email_address;
    private String password;

    public StudentDto() {
    }

    public static StudentDto fromDomain(Student student) {
        StudentDto dto = new StudentDto();
        dto.setId(student.getId());
        dto.setFirst_name(student.getFirst_name());
        dto.setLast_name(student.getLast_name());
        dto.setPseudonym(student.getPseudonym());
        dto.setEmail_address(student.getEmail_address());
        dto.setPassword(student.getPassword());

        return dto;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Student toDomain() {
        Student student = new Student();
        student.setId(this.id);
        student.setFirst_name(this.first_name);
        student.setLast_name(this.last_name);
        student.setPseudonym(this.pseudonym);
        student.setEmail_address(this.email_address);
        student.setPassword(this.password);

        return student;
    }

}
