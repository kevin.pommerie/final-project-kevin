package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Category;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.exception.NotFoundException;
import com.example.finalprojectback.repository.CategoryRepository;
import com.example.finalprojectback.repository.QuestionRepository;
import com.example.finalprojectback.service.CategoryService;
import com.example.finalprojectback.service.QuestionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static com.example.finalprojectback.repository.CategorySpecifications.*;

@CrossOrigin
@RestController
public class CategoryController {
    private final CategoryRepository categoryRepository;
    private final CategoryService categoryService;
    private final QuestionRepository questionRepository;

    public CategoryController(CategoryRepository categoryRepository, CategoryService categoryService, QuestionRepository questionRepository) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
        this.questionRepository = questionRepository;
    }

    private static Specification<Category> fromFilter(CategoryFilterDto filter) {
        return hasId(filter.getId())
                .and(hasName(filter.getName()));
    }

    @GetMapping("/categories")
    public PageDto<CategoryDto> getCategories(@SortDefault(value = {"id"}) Pageable pageable, CategoryFilterDto filter) {
        //Specification<Category> categorySpecification = fromFilter(filter);
        return PageDto.fromDomain(categoryRepository.findAll(pageable), CategoryDto::fromDomain);
    }

    @GetMapping("/categories/{id}")
    public CategoryDto getCategoryById(@PathVariable Integer id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Category.class, id));

        return CategoryDto.fromDomain(category);
    }

    @PostMapping("/categories")
    public ResponseEntity<CategoryDto> addCategory(@RequestBody CategoryDto categoryDto) {
        Category newCategory = categoryService.addCategory(categoryDto);

        URI location = URI.create("/categories" + newCategory.getId());
        return ResponseEntity.created(location)
                .body(CategoryDto.fromDomain(newCategory));
    }

    @DeleteMapping("/categories/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategory(@PathVariable Integer id) {
        categoryService.deleteCategory(id);
    }

    @GetMapping("/categories/{id}/questions")
    public List<Question> getQuestionListByCategory(@PathVariable Integer id) {
        /*CategoryDto newCategoryDto = getCategoryById(id);
        return PageDto.fromDomain(questionRepository.findQuestionByCategory(newCategoryDto), QuestionDto::fromDomain);
    */
        Category category = getCategoryById(id).toDomain();
        return questionRepository.findQuestionByCategory(category);
    }
}