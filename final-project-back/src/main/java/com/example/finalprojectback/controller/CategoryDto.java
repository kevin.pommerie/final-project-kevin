package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Category;

public class CategoryDto {
    private int id;
    private String name;

    public CategoryDto(){}

    public static CategoryDto fromDomain(Category category){
        CategoryDto dto = new CategoryDto();
        dto.setId(category.getId());
        dto.setName(category.getName());

        return dto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category toDomain(){
        Category category = new Category();
        category.setId(this.id);
        category.setName(this.name);

        return category;
    }
}
