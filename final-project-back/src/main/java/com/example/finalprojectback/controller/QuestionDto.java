package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Category;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.domain.Student;

public class QuestionDto {

    private int id;
    private String title;
    private String content;
    private Category category;
    private Student student;

    public QuestionDto(){

    }

    public static QuestionDto fromDomain(Question question){
        QuestionDto dto = new QuestionDto();
        dto.setId(question.getId());
        dto.setTitle(question.getTitle());
        dto.setContent(question.getContent());
        dto.setCategory(question.getCategory());
        dto.setStudent(question.getStudent());

        return dto;
    }

    public Integer getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Question toDomain() {
        Question question = new Question();
        question.setId(this.id);
        question.setTitle(this.title);
        question.setContent(this.content);
        question.setCategory(this.category);
        question.setStudent(this.student);

        return question;
    }

}
