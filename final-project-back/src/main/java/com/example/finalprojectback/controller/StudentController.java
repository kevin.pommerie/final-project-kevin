package com.example.finalprojectback.controller;

import com.example.finalprojectback.domain.Student;
import com.example.finalprojectback.exception.NotFoundException;
import com.example.finalprojectback.repository.StudentRepository;
import com.example.finalprojectback.service.StudentService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@CrossOrigin
@RestController
public class StudentController {

    private final StudentRepository studentRepository;
    private final StudentService studentService;

    public StudentController(StudentRepository studentRepository, StudentService studentService){
        this.studentRepository = studentRepository;
        this.studentService = studentService;
    }

    @GetMapping("/students")
    public PageDto<StudentDto> getStudents(@SortDefault(value = {"id"}) Pageable pageable) {
        return PageDto.fromDomain(studentRepository.findAll(pageable), StudentDto::fromDomain);
    }

    @GetMapping("/students/{id}")
    public StudentDto getStudentById(@PathVariable Integer id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Student.class, id));

        return StudentDto.fromDomain(student);
    }

    @PostMapping("/students")
    public ResponseEntity<StudentDto> addStudent(@RequestBody StudentDto studentDto) {
        Student newStudent = studentService.addStudent(studentDto);

        URI location = URI.create("/students" + newStudent.getId());
        return ResponseEntity.created(location)
                .body(StudentDto.fromDomain(newStudent));
    }

    @DeleteMapping("/students/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Integer id) { studentService.deleteStudent(id); }

}
