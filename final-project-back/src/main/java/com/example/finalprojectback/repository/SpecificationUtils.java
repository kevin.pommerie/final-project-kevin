package com.example.finalprojectback.repository;

import org.springframework.data.jpa.domain.Specification;

/**
 * Utility class to facilitate {@link Specification} usage.
 */
public interface SpecificationUtils {
    public static <V, E> Specification<E> hasAttributeEquals(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(attribute), value);
        }
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeGreaterThanOrEqualsTo(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(attribute), value);
        }
    }

    public static <V extends Comparable<? super V>, E> Specification<E> hasAttributeLessThan(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return (root, query, criteriaBuilder) -> criteriaBuilder.lessThan(root.get(attribute), value);
        }
    }
}
