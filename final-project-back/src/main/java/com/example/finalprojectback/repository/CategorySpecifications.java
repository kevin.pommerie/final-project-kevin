package com.example.finalprojectback.repository;

import com.example.finalprojectback.controller.CategoryDto;
import com.example.finalprojectback.domain.Category;
import org.springframework.data.jpa.domain.Specification;

import static com.example.finalprojectback.repository.SpecificationUtils.hasAttributeEquals;

/**
 * {@link Specification}s for {@link CategoryDto} entity.
 */
public interface CategorySpecifications {
    static Specification<Category> hasId(Integer id) {
        return hasAttributeEquals("id", id);
    }

    static Specification<Category> hasName(String name) { return hasAttributeEquals("name", name); }
}
