package com.example.finalprojectback.repository;

import com.example.finalprojectback.controller.CategoryDto;
import com.example.finalprojectback.controller.PageDto;
import com.example.finalprojectback.controller.QuestionDto;
import com.example.finalprojectback.domain.Category;
import com.example.finalprojectback.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer>, JpaSpecificationExecutor<Question> {
    List<Question> findQuestionByCategory(Category category);
}
