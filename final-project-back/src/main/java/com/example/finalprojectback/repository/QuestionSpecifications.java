package com.example.finalprojectback.repository;

import com.example.finalprojectback.controller.QuestionDto;
import com.example.finalprojectback.domain.Category;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.domain.Student;
import org.springframework.data.jpa.domain.Specification;

import java.time.Instant;

import static com.example.finalprojectback.repository.SpecificationUtils.hasAttributeEquals;

/**
 * {@link Specification}s for {@link QuestionDto} entity.
 */
public interface QuestionSpecifications {
    static Specification<Question> hasId(Integer id) {
        return hasAttributeEquals("id", id);
    }

    static Specification<Question> hasTitle(String title) { return hasAttributeEquals("title", title); }

    static Specification<Question> hasContent(String content) { return hasAttributeEquals("content", content); }

    static Specification<Question> hasCategory(Category category) { return hasAttributeEquals("Category", category); }

    static Specification<Question> hasStudent(Student student) { return hasAttributeEquals("Student", student); }
}
