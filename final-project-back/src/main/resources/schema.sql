DROP TABLE IF EXISTS votes;
DROP TABLE IF EXISTS answers;
DROP TABLE IF EXISTS questions;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS students;

CREATE TABLE IF NOT EXISTS students
(
    id integer generated always as identity primary key,
    first_name text not null,
    last_name text not null,
    pseudonym text not null unique,
    email_address text not null unique,
    password text not null
);

create index if not exists students_pseudonym_idx on students (pseudonym);

CREATE TABLE IF NOT EXISTS categories
(
    id integer generated always as identity primary key,
    name text not null
);

create index if not exists categories_name_idx on categories (name);

CREATE TABLE IF NOT EXISTS questions
(
    id integer generated always as identity primary key,
    title text not null,
    content text not null,
    id_category integer references categories (id),
    id_student integer references students (id)
);

create index if not exists questions_title_idx on questions (title);
create index if not exists questions_id_category_idx on questions (id_category);
create index if not exists questions_id_student_idx on questions (id_student);

CREATE TABLE IF NOT EXISTS answers
(
    id integer generated always as identity primary key,
    id_student integer references students (id),
    id_question integer references  questions (id),
    content text not null
);

create index if not exists answers_id_student_idx on answers (id_student);
create index if not exists answers_id_question_idx on answers (id_question);

CREATE TABLE IF NOT EXISTS votes
(
    id integer generated always as identity primary key,
    id_answer integer references answers (id),
    votevalue integer not null
);

create index if not exists votes_votevalue_idx on votes (votevalue);



INSERT INTO categories(name)
    VALUES ('git'),
           ('java'),
           ('javascript'),
           ('html'),
           ('css'),
           ('php'),
           ('python'),
           ('agile'),
           ('security'),
           ('other');

INSERT INTO students(first_name, last_name, pseudonym, email_address, password)
    VALUES ('admin', 'admin', 'admin', 'admin@admin.com', 'password'),
           ('first_name', 'last_name', 'testuser', 'fake@mail.com', 'password');



/* ///////////////////////////////////////////////////////////////////////////// */

INSERT INTO questions(title, content, id_category, id_student)
    VALUES ('how do i click a button ?', 'Hi folks, Im new to this computer thing and I dont know how to click... :(', 10, 1),
           ('how do i rollback a commit ?', 'I misclicked and now I want to rollback, please send help :crying_emoji', 1, 1),
           ('how do i push a commit ?', 'i know it is basic, but this question serves in reality as a test for displaying the questions in the front-end of this project', 1, 2);

INSERT INTO answers(id_student, id_question, content)
    VALUES (1, 1, 'this is how you do it'),
           (1, 1, 'or you can do it like this if you prefer');