-- Drop all data into these tables
delete from votes;
delete from answers;
delete from questions;
delete from categories;
delete from students;