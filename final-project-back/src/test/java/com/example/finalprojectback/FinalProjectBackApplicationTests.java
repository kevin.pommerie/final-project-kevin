package com.example.finalprojectback;

import com.example.finalprojectback.domain.Answer;
import com.example.finalprojectback.domain.Question;
import com.example.finalprojectback.it.IntegrationTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@IntegrationTest
class FinalProjectApplicationIT {

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void contextLoads() throws JsonProcessingException {
        System.out.println(objectMapper.writeValueAsString(new Question()));
    }

    @Test
    void listOfString() throws JsonProcessingException {
        System.out.println(objectMapper.writeValueAsString(new ListOfString()));
    }

    public static class ListOfString {
        private List<Answer> a = null;

        public List<Answer> getA() {
            return a;
        }

        public void setA(List<Answer> a) {
            this.a = a;
        }
    }
}
