package com.example.finalprojectback.it;

import com.example.finalprojectback.domain.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Test utility class to create easily test dataset.
 * Handle entities order creation to avoid the developer to care about it.
 */
@Component
public class EntitiesPersister {

    /**
     * Order in which {@link javax.persistence.Entity} must be persist into database.
     */
    private static final List<Class<?>> ENTITIES_PERSISTENCE_ORDER = List.of(
            Question.class,
            Answer.class
    );

    private final EntityManager entityManager;

    public EntitiesPersister(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Persist all entities by respecting order creation.
     *
     * @param entities Entities to persist
     */
    @Transactional
    public void persist(Object... entities) {
        Arrays.stream(entities)
                .sorted(Comparator.comparing(entity -> ENTITIES_PERSISTENCE_ORDER.indexOf(entity.getClass())))
                .forEach(entityManager::persist);
    }
}