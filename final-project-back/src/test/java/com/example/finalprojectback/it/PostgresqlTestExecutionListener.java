package com.example.finalprojectback.it;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.util.Map;
import java.util.UUID;

/**
 * {@link TestExecutionListener} that start a new Postgres instance into a Docker container for integration tests.
 */
public class PostgresqlTestExecutionListener implements TestExecutionListener {

    private final Logger LOGGER = LoggerFactory.getLogger(PostgresqlTestExecutionListener.class);

    // Postgres container attributes
    private static final int POSTGRESQL_DEFAULT_PORT = 5432;
    private static final GenericContainer<?> postgresqlContainer;

    // Postgres database attributes
    public static final String DB_NAME = "postgres_" + UUID.randomUUID();
    public static final String DB_USERNAME = "postgres";
    public static final String DB_PASSWORD = "mysecretpassword";

    /**
     * Script to clean-up database before each test
     */
    public static final ResourceDatabasePopulator DB_CLEANUP_SCRIPT = new ResourceDatabasePopulator(new ClassPathResource("truncate.sql"));

    static {
        // Start a new Postgres container
        postgresqlContainer = new PostgreSQLContainer<>("postgres:13.4-alpine")
                .withDatabaseName(DB_NAME)
                .withUsername(DB_USERNAME)
                .withPassword(DB_PASSWORD)
                .withTmpFs(Map.of("/var/lib/postgresql/data", ""));
        postgresqlContainer.start();

        // Override Spring properties to use the new Postgres instance
        String jdbcUrl = "jdbc:postgresql://" + postgresqlContainer.getContainerIpAddress() + ":" + postgresqlContainer.getMappedPort(POSTGRESQL_DEFAULT_PORT) + "/" + DB_NAME;
        System.out.println("JDBC URL : " + jdbcUrl);
        System.setProperty("spring.datasource.url", jdbcUrl);
        System.setProperty("spring.datasource.username", DB_USERNAME);
        System.setProperty("spring.datasource.password", DB_PASSWORD);
    }

    @Override
    public void beforeTestMethod(TestContext testContext) {
        LOGGER.info("Initialize database before test...");
        DataSource postgresDatasource = testContext.getApplicationContext().getBean(DataSource.class);
        DB_CLEANUP_SCRIPT.execute(postgresDatasource);
    }
}