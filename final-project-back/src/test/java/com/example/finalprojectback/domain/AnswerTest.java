package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidAnswerException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AnswerTest {
    @Test
    void shouldCreateAnswer() {
        Answer answer = AnswerFactory.solution();
        assertThat(answer).isNotNull();
    }

    @Test
    void shouldRejectNullStudent(){
        assertThatThrownBy(() -> new Answer().setStudent(null))
                .isInstanceOf(InvalidAnswerException.class);
    }

    @Test
    void shouldRejectNullQuestion(){
        assertThatThrownBy(() -> new Answer().setQuestion(null))
                .isInstanceOf(InvalidAnswerException.class);
    }

    @Test
    void shouldRejectEmptyContent(){
        assertThatThrownBy(() -> new Answer().setContent(""))
                .isInstanceOf(InvalidAnswerException.class);
    }
}
