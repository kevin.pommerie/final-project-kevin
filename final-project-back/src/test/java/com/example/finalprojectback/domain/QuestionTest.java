package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidQuestionException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class QuestionTest {
    @Test
    void shouldCreateQuestion() {
        Question question = QuestionFactory.howdoi();
        assertThat(question).isNotNull();
    }

    @Test
    void shouldRejectEmptyContent(){
        assertThatThrownBy(() -> new Question().setContent(""))
                .isInstanceOf(InvalidQuestionException.class);
    }

    @Test
    void shouldRejectEmptyTitle(){
        assertThatThrownBy(() -> new Question().setTitle(""))
                .isInstanceOf(InvalidQuestionException.class);
    }

    @Test
    void shouldRejectNullCategory(){
        assertThatThrownBy(() -> new Question().setCategory(null))
                .isInstanceOf(InvalidQuestionException.class);
    }

    @Test
    void shouldRejectNullStudent(){
        assertThatThrownBy(() -> new Question().setStudent(null))
                .isInstanceOf(InvalidQuestionException.class);
    }
}
