package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidCategoryException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CategoryTest {
    @Test
    void shouldCreateCategory() {
        Category category = CategoryFactory.testCategory();
        assertThat(category).isNotNull();
    }

    @Test
    void shouldRejectEmptyName(){
        assertThatThrownBy(() -> new Category().setName(""))
                .isInstanceOf(InvalidCategoryException.class);
    }
}
