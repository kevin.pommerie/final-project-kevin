package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidStudentException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class StudentTest {
    @Test
    void shouldCreateStudent() {
        Student student = StudentFactory.testuser();
        assertThat(student).isNotNull();
    }

    @Test
    void shouldRejectEmptyFirstName() {
        assertThatThrownBy(() -> new Student().setFirst_name(""))
                .isInstanceOf(InvalidStudentException.class);
    }

    @Test
    void shouldRejectEmptyLastName() {
        assertThatThrownBy(() -> new Student().setLast_name(""))
                .isInstanceOf(InvalidStudentException.class);
    }

    @Test
    void shouldRejectEmptyPseudonym() {
        assertThatThrownBy(() -> new Student().setPseudonym(""))
                .isInstanceOf(InvalidStudentException.class);
    }

    @Test
    void shouldRejectEmptyPassword() {
        assertThatThrownBy(() -> new Student().setPassword(""))
                .isInstanceOf(InvalidStudentException.class);
    }

    @Test
    void shouldRejectEmptyEmail_address() {
        assertThatThrownBy(() -> new Student().setEmail_address(""))
                .isInstanceOf(InvalidStudentException.class);
    }
}
