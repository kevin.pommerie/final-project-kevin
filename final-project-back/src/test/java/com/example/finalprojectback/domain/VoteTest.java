package com.example.finalprojectback.domain;

import com.example.finalprojectback.exception.InvalidVoteException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class VoteTest {
    @Test
    void shouldCreateVote() {
        Vote vote = VoteFactory.testvote();
        assertThat(vote).isNotNull();
    }

    @Test
    void shouldRejectNullAnswer() {
        assertThatThrownBy(() -> new Vote().setAnswer(null))
                .isInstanceOf(InvalidVoteException.class);
    }

    @Test
    void shouldRejectWrongVotevalue() {
        assertThatThrownBy(() -> new Vote().setVotevalue(0))
                .isInstanceOf(InvalidVoteException.class);
    }
}
