import {Component, OnInit} from '@angular/core';
import {Question} from "../interface/Question";
import {Category} from "../interface/Category";
import {Observable} from "rxjs";
import {Student} from "../interface/Student";
import {ConfigService} from "../config.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../Service/category.service";
import {QuestionService} from "../Service/question.service";
import {Answer} from "../interface/Answer";
import {StudentService} from "../Service/student.service";
import {AnswerService} from "../Service/answer.service";

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.css']
})
export class QuestionDetailsComponent implements OnInit {
  questionObservable!: Observable<Question>;
  question!: Question;
  category!: Category;
  student!: Student;
  questionId!: number;
  answer!: Answer;
  answersList!: Answer[];
  routeParams!: any;

  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private questionService: QuestionService,
    private studentService: StudentService,
    private categoryService: CategoryService,
    private answersService: AnswerService
  ) {
  }

  ngOnInit(): void {
    this.routeParams = this.route.snapshot.paramMap;
    this.questionId = Number(this.routeParams.get('id'));

    this.questionObservable = this.questionService.getQuestion(this.questionId);

    this.questionService.getQuestion(this.questionId)
      .subscribe((question) => {
        this.question = question;

        this.categoryService.getCategory(this.question.category.id)
          .subscribe((category) => {
            this.category = category;

            this.studentService.getStudent(this.question.student.id)
              .subscribe((student) => {
                this.student = student;

                this.answersService.getAnswerListByQuestion(this.questionId)
                  .subscribe((answersList) => {
                    this.answersList = answersList;
                  });
              });
          });
      });
  }

  addAnswerToQuestion(answer: Answer) {
    /*this.questionService.addAnswerToQuestion(answer).subscribe(
      () => this.router.navigate(['/questions']));*/
  }
}
