import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Page} from "../interface/Page";
import {Category} from "../interface/Category";
import {ConfigService} from "../config.service";
import {Router} from "@angular/router";
import {CategoryService} from "../Service/category.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  page: Observable<Page<any>> | undefined
  categories: Observable<Category[]> | undefined

  constructor(
    private configService: ConfigService,
    private router: Router,
    private categoriesService: CategoryService
  ) {
  }

  ngOnInit(): void {
    this.page = this.categoriesService.getCategoryList();
    this.categories = this.page.pipe(map(page => page.content));
  }

  displayPageContent(pageToDisplay: number) {
    //Get new page to display
    this.page = this.categoriesService.getCategoryListPage(pageToDisplay);

    //Get list of Observable
    this.categories = this.page.pipe(map(page => page.content));
  }

  detailsCategory(id: number) {
    this.router.navigate(['categories/' + id]);
  }

  changePage(currentPage: number) {
    this.page = this.categoriesService.getCategoryListPage(currentPage)
    this.categories = this.page.pipe(map(page => {
      return page.content
    }))
  }

}
