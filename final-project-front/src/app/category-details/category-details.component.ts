import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Category} from "../interface/Category";
import {ConfigService} from "../config.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../Service/category.service";
import {Question} from "../interface/Question";

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {
  categoryObservable!: Observable<Category>;
  category!: Category;
  categoryId!: number;
  questionsList!: Question[];
  routeParams!: any;

  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.routeParams = this.route.snapshot.paramMap;
    this.categoryId = Number(this.routeParams.get('id'));

    this.categoryObservable = this.categoryService.getCategory(this.categoryId);

    this.categoryService.getCategory(this.categoryId)
      .subscribe((category) => {
        this.category = category;

        this.categoryService.getQuestionListByCategory(this.categoryId)
          .subscribe((questionsList) => {
            this.questionsList = questionsList;
          });
      });
  }
}
