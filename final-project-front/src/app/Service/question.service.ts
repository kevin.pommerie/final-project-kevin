import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Question} from "../interface/Question";
import {Page} from "../interface/Page";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private httpClient: HttpClient) { }

  url: string = "http://localhost:8080/"

  getQuestion(id: number) {
    return this.httpClient.get<Question>(this.url + "questions/" + id);
  }

  getQuestionList() {
    return this.httpClient.get<Page<Question>>(this.url + 'questions');
  }

  getQuestionListPage(page: number) {
    return this.httpClient.get<Page<Question>>(this.url + "questions?page=" + page);
  }

  newQuestion(question: Question) {
    return this.httpClient.post<Question>(this.url + "questions", question);
  }

  deleteQuestion(id: number) {
    return this.httpClient.delete<Question>(this.url + "questions/" + id);
  }

  updateQuestion(updatedQuestion: Question) {
    return this.httpClient.put<Question>(this.url + "questions/" + updatedQuestion.id, updatedQuestion);
  }
}
