import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Page} from "../interface/Page";
import {Student} from "../interface/Student";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private httpClient: HttpClient) { }

  url: string = "http://localhost:8080/"

  getStudent(id: number) {
    return this.httpClient.get<Student>(this.url + "students/" + id);
  }

  getStudentList() {
    return this.httpClient.get<Page<Student>>('http://localhost:8080/students');
  }

  getStudentListPage(page: number) {
    return this.httpClient.get<Page<Student>>(this.url + "students?page=" + page);
  }

  newStudent(student: Student) {
    return this.httpClient.post<Student>(this.url + "students", student);
  }

  deleteStudent(id: number) {
    return this.httpClient.delete<Student>(this.url + "students/" + id);
  }

  updateStudent(updatedStudent: Student) {
    return this.httpClient.put<Student>(this.url + "students/" + updatedStudent.id, updatedStudent);
  }
}
