import {Injectable} from '@angular/core';
import {Student} from "../interface/Student";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly SESSION_STORAGE_KEY = 'currentStudent';

  constructor() {
  }

  login(student: Student) {
    console.log('log');
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(student));
  }

  logout() {
    console.log('logout');
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
  }

  getCurrentStudentBasicAuthentication(): string | undefined {
    const currentStudentPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentStudentPlain) {
      const currentStudent = JSON.parse(currentStudentPlain);
      return "Basic " + btoa(currentStudent.pseudonym + ":" + currentStudent.password);
    } else {
      return undefined;
    }
  }

  getConnectedStudent(){
    const currentStudentPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentStudentPlain) {
      const currentStudent = JSON.parse(currentStudentPlain);
      return currentStudent.pseudonym;
    }
  }
}
