import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Page} from "../interface/Page";
import {Category} from "../interface/Category";
import {Question} from "../interface/Question";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }

  url: string = "http://localhost:8080/"

  getCategory(id: number) {
    return this.httpClient.get<Category>(this.url + "categories/" + id);
  }

  getCategoryList() {
    return this.httpClient.get<Page<Category>>('http://localhost:8080/categories');
  }

  getCategoryListPage(page: number) {
    return this.httpClient.get<Page<Category>>(this.url + "categories?page=" + page);
  }

  newCategory(category: Category) {
    return this.httpClient.post<Category>(this.url + "categories", category);
  }

  deleteCategory(id: number) {
    return this.httpClient.delete<Category>(this.url + "categories/" + id);
  }

  updateCategory(updatedCategory: Category) {
    return this.httpClient.put<Category>(this.url + "categories/" + updatedCategory.id, updatedCategory);
  }

  getQuestionListByCategory(category_id: number) {
    return this.httpClient.get<Question[]>(this.url + 'categories/' + category_id + '/questions');
  }
}
