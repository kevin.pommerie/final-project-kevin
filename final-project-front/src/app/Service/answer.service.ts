import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Page} from "../interface/Page";
import {Answer} from "../interface/Answer";

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private httpClient: HttpClient) { }

  url: string = "http://localhost:8080/"

  getAnswer(id: number) {
    return this.httpClient.get<Answer>(this.url + "answers/" + id);
  }

  getAnswersList() {
    return this.httpClient.get<Page<Answer>>('http://localhost:8080/answers');
  }

  getAnswerListPage(page: number) {
    return this.httpClient.get<Page<Answer>>(this.url + "answers?page=" + page);
  }

  newAnswer(answer: Answer) {
    return this.httpClient.post<Answer>(this.url + "answers", answer);
  }

  deleteAnswer(id: number) {
    return this.httpClient.delete<Answer>(this.url + "answers/" + id);
  }

  updateAnswer(updatedAnswer: Answer) {
    return this.httpClient.put<Answer>(this.url + "answers/" + updatedAnswer.id, updatedAnswer);
  }

  getAnswerListByQuestion(id: number){
    return this.httpClient.get<Answer[]>(this.url + "questions/" + id + "/answers");
  }
}
