import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-badrouting',
  templateUrl: './badrouting.component.html',
  styleUrls: ['./badrouting.component.css']
})
export class BadroutingComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {  location.replace('http://localhost/'); }, 2000);
  }
}
