import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {LoginComponent} from "./login/login.component";
import {BadroutingComponent} from "./badrouting/badrouting.component";
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BasicAuthInterceptor, NoopInterceptor} from "./interceptors";
import {QuestionListComponent} from './question-list/question-list.component';
import {QuestionDetailsComponent} from './question-details/question-details.component';
import { StudentComponent } from './student/student.component';
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import { CategoryDetailsComponent } from './category-details/category-details.component';
import { CategoryListComponent } from './category-list/category-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BadroutingComponent,
    QuestionListComponent,
    QuestionDetailsComponent,
    StudentComponent,
    HeaderComponent,
    FooterComponent,
    CategoryDetailsComponent,
    CategoryListComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: QuestionListComponent},
      {path: 'questions', component: QuestionListComponent},
      {path: 'questions/:id', component: QuestionDetailsComponent},
      {path: 'categories', component: CategoryListComponent},
      {path: 'categories/:id', component: CategoryDetailsComponent},
      {path: 'login', component: LoginComponent},
      {path: 'profile', component: StudentComponent},
      {path: '**', component: BadroutingComponent}
    ]),
    ReactiveFormsModule, FormsModule, BrowserAnimationsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: NoopInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
