import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Page} from "./interface/Page";
import {Product} from "./interface/Product";
import {Category} from "./interface/Category";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient) {}

  url: string = "http://localhost:8080/"

  getListQuestion(pageToDisplay: number) {
    return this.http.get<Page<Question>>(this.url + "questions?page=" + pageToDisplay, {headers: {Authorization: "Basic dXNlcjpwYXNzd29yZA=="}})
  }

  getDetailsQuestion(id: number) {
    return this.http.get<Question>(this.url + "questions/" + id)
  }

  getDetailsCategory(id: number) {
    return this.http.get<Category>(this.url + "categories/" + id)
  }
}
