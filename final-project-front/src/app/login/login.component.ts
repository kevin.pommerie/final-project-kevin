import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../Service/authentication.service";

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  returnUrl: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    this.form = this.formBuilder.group({
      pseudonym: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.form.reset();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    this.submitted = true;

    // stops here if form is invalid
    if (this.form.invalid) {
      return;
    }
    this.authenticationService.login({
      pseudonym: this.form.controls.pseudonym.value,
      password: this.form.controls.password.value,
      id: 0,
      first_name: "",
      last_name: "",
      email_address: ""
    });
    this.router.navigate([this.returnUrl]);
  }
}
