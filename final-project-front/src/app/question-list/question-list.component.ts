import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Page} from "../interface/Page";
import {Question} from "../interface/Question";
import {Router} from "@angular/router";
import {map} from "rxjs/operators";
import {ConfigService} from "../config.service";
import {QuestionService} from "../Service/question.service";

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {

  page: Observable<Page<any>> | undefined
  questions: Observable<Question[]> | undefined

  constructor(
    private configService: ConfigService,
    private router: Router,
    private questionsService: QuestionService
  ) { }

  ngOnInit(): void {
    this.page = this.questionsService.getQuestionList();
    this.questions = this.page.pipe(map(page => page.content));
  }

  displayPageContent(pageToDisplay: number) {
    //Get new page to display
    this.page = this.questionsService.getQuestionListPage(pageToDisplay);

    //Get list of Observable
    this.questions = this.page.pipe(map(page => page.content));
  }

  detailsQuestion(id: number){
    this.router.navigate(['questions/' + id]);
  }

  changePage(currentPage: number) {
    this.page = this.questionsService.getQuestionListPage(currentPage)
    this.questions = this.page.pipe(map(page => {
      return page.content
    }))
  }

}
