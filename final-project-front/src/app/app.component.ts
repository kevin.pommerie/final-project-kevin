import { Component } from '@angular/core';
import {AuthenticationService} from "./Service/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'final-project-front';

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  logoutAndRedirect(): void {
    this.authenticationService.logout()
    this.router.navigate(['/login']);
  }
}
