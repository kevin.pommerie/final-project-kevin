import {Student} from "./Student";
import {Question} from "./Question";

export interface Answer {
  id: number
  student: Student
  question: Question
  content: string
}
