export interface Page<Product> {
  content: Array<Product>
  page: number
  size: number
  count: number
  totalCount: number
  totalPage: number
}
