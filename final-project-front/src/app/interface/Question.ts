import {Category} from "./Category";
import {Student} from "./Student";

export interface Question {
  id: number
  title: string
  content: string
  category: Category
  student: Student
}
