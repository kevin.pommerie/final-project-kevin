export interface Student {
  id: number
  first_name: string
  last_name: string
  pseudonym: string
  password: string
  email_address: string
}
