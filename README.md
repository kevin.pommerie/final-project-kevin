# final-project_kevin



## Getting started

Please make sure you have Docker, Maven and nodeJS installed.

To build the project, run the file build.sh (with Linux terminal) or 
build.ps1 (with PowerShell command prompt on Windows) in the root repository of the project.

Then use the command "docker-compose up" in the project root repository to launch.

The back-end of the project will be running on localhost:8080 and the front-end on localhost:80.


## License
This project is open-source and free, without a specific licence. Use it as you want.

## Project status
On hold, maybe forever.

## Known issues
- The /profile route does not show all the wanted data about the connected user.

## Test users
- admin : password
- user : password